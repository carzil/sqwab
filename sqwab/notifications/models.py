from django.db import models
from sqwab.core.models.user import User

class Notification(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    header = models.TextField()
    text = models.TextField()
    level = models.TextField()
