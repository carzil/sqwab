from django.conf.urls.defaults import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

js_info_dict = {
    "domain": "djangojs",
    "packages": ("sqwab",),
}

urlpatterns = patterns("",
    #----- MAIN -----#
    url(r"^$", "sqwab.core.views.main.get_main"),
    url(r"^about/$", "sqwab.core.views.main.get_about"),

    #----- AUTH SYSTEM -----#
    url(r"^login/$", "sqwab.core.views.user.log_in"),
    url(r"^logout/$", "sqwab.core.views.user.log_out"),
    url(r"^registration/$", "sqwab.core.views.user.registration"),

    #----- PROFILE -----#
    url(r"^profile/$", "sqwab.core.views.user.get_profile"),
    url(r"^profile/(\d*)/$", "sqwab.core.views.user.get_profile"),
    url(r"^profile/edit/$", "sqwab.core.views.user.edit_profile"),

    url(r"^profile/friends/$", "sqwab.core.views.friends.get_friends"),
    url(r"^profile/(\d*)/friends/$", "sqwab.core.views.friends.get_friends"),

    #----- BLOG -----#
    url(r"^blog/$", "sqwab.core.views.blog.get_blog"),
    url(r"^blog/(\d*)/$", "sqwab.core.views.blog.get_blog"),
    url(r"^blog/edit/$", "sqwab.core.views.blog.edit_blog"),
    url(r"^blog/(\d*)/edit/$", "sqwab.core.views.blog.edit_blog"),

    #----- POST -----#
    url(r"^blog/(?P<bid>\d*)/post/(?P<pid>\d*)/$", "sqwab.core.views.blog.get_post"),

    #----- POST - editing -----#
    url(r"^blog/p/(\d*)/edit/$", "sqwab.core.views.blog.edit_post"),
    url(r"^blog/post/(\d*)/edit/$", "sqwab.core.views.blog.edit_post"),

    url(r"^blog/l/(\d*)/edit/$", "sqwab.core.views.blog.edit_link"),
    url(r"^blog/link/(\d*)/edit/$", "sqwab.core.views.blog.edit_link"),

    url(r"^blog/i/(\d*)/edit/$", "sqwab.core.views.blog.edit_image"),
    url(r"^blog/image/(\d*)/edit/$", "sqwab.core.views.blog.edit_image"),

    url(r"^blog/(?P<bid>\d*)/add/post/$", "sqwab.core.views.blog.add_post"),
    url(r"^blog/(?P<bid>\d*)/add/link/$", "sqwab.core.views.blog.add_link"),
    url(r"^blog/add/post/$", "sqwab.core.views.blog.add_post_default"),
    url(r"^blog/add/image/$", "sqwab.core.views.blog.add_image_default"),
    url(r"^blog/add/link/$", "sqwab.core.views.blog.add_link_default"),
    url(r"^blog/post/(?P<pid>\d*)/$", "sqwab.core.views.blog.get_post"),

    url(r"^test/$", "sqwab.core.views.test.get_test"),

    url(r"^away/$", "sqwab.core.views.main.away"),

    url(r"^search/", "sqwab.core.views.search.search"),

    url(r"^settings/$", "sqwab.core.views.usettings.usettings"),

    url(r"^notifications/$", "sqwab.api.views.notifications.get_all"),
    url(r"^notifications/add/$", "sqwab.api.views.notifications.add"),

    url(r"^json/", include("sqwab.api.urls")),

    url(r"^admin/doc/", include("django.contrib.admindocs.urls")),
    url(r"^admin/", include(admin.site.urls)),

    url(r"^jsi18n/$", "django.views.i18n.javascript_catalog", js_info_dict),

    url(r"^captcha/", "sqwab.core.views.captcha.get_captcha"),
)


urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
