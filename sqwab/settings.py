# -*- coding=utf-8 -*-
import os, sys
import djcelery

djcelery.setup_loader()

sys.path.insert(0, os.path.abspath(".."))

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = ()

MANAGERS = ADMINS

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "Sqwab",
        "USER": "postgres",
        "PASSWORD": "sqwab",
        "HOST": "localhost",
        "PORT": "5432",
    }
}

TIME_ZONE = "Europe/Moscow"

LOGIN_URL = "/login/"

LANGUAGE_CODE = "ru"

SITE_ID = 1

USE_I18N = True

USE_L10N = True

MEDIA_ROOT = os.path.join(PROJECT_ROOT, "media")

MEDIA_URL = "/media/"

STATIC_ROOT = os.path.join(PROJECT_ROOT, "static")

STATIC_URL = "/static/"

ADMIN_MEDIA_PREFIX = "/static/admin/"

STATICFILES_DIRS = ()


STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

SECRET_KEY = "wy@wrt9jrxfnjd6m#3yz7f#87+_$tw$yz3kubl5*20y96&@hi+"

TEMPLATE_LOADERS = (
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
)

MIDDLEWARE_CLASSES = (
#    "django.middleware.cache.UpdateCacheMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
#    "django.middleware.cache.FetchFromCacheMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "sqwab.pagination.middleware.PaginationMiddleware",
)

ROOT_URLCONF = "sqwab.urls"

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, "templates"),
)

INSTALLED_APPS = (
    "sqwab",
    "sqwab.core",
    "sqwab.api",
    "sqwab.notifications",
    "sqwab.pagination",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.admin",
    "django.contrib.admindocs",
    "django.contrib.markup",
    "south",
    "haystack",
    "mptt",
    "djcelery",
    "djkombu",

)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.messages.context_processors.messages",
    "sqwab.core.views.utils.pm_context_processor",
    "django.core.context_processors.request",
    "django.core.context_processors.csrf",
)

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "class": "django.utils.log.AdminEmailHandler"
        }
    },
    "loggers": {
        "django.request": {
            "handlers": ["mail_admins"],
            "level": "ERROR",
            "propagate": True,
        },
    }
}

if not DEBUG:
    CACHES = {
        "default": {
            "BACKEND": "redis_cache.RedisCache",
            "LOCATION": "127.0.0.1:6379",
            "OPTIONS": {
                "DB": 1,
                "PARSER_CLASS": "redis.connection.PythonParser"
            },
        }
    }
else:
    CACHES = {
        "default": {
            "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
            "LOCATION": "sqwab-unique"
        }
    }

CACHE_TIME = 60 * 60

PAGINATION_DEFAULT_PAGINATION = 7

HAYSTACK_CONNECTIONS = {
    "default": {
        "ENGINE": "haystack.backends.whoosh_backend.WhooshEngine",
        "PATH": os.path.join(PROJECT_ROOT, "whoosh_index")
    },
}

LANGUAGES = (
    ("ru", u"Русский"),
    ("en", "English"),
)

CAPTCHA_BGCOLORS = (
    (230, 230, 230),
)

CAPTCHA_SIZE = (
    160, 50
)

CAPTCHA_FONTS = map(
    lambda x: os.path.join(PROJECT_ROOT, "fonts", x),
    filter(
        lambda x: x.endswith(".ttf"),
        os.listdir(os.path.join(PROJECT_ROOT, "fonts"))
    )
)


CAPTCHA_TEXT_COLORS = (
    (255, 204, 51),
    (255, 0, 0),
    (0, 0, 255),
    (204, 0, 102),
    (204, 0, 204),
    (204, 51, 204),
    (102, 0, 204),
    (0, 102, 0),
    (51, 102, 0),
    (0, 0, 0)
)

CAPTCHA_TEXT_LENGTH = 5

CAPTCHA_TEXT_SIZE = 34

CAPTCHA_TEXT_MAX_ROTATION = 20
CAPTCHA_TEXT_MIN_ROTATION = -20

CAPTCHA_MAX_LETTER_DISTANCE = 3
CAPTCHA_MIN_LETTER_DISTANCE = 2

CAPTCHA_Y_PROBABILITY_SHIFT = 3

CAPTCHA_MAX_Y_SHIFT = 10
CAPTCHA_MIN_Y_SHIFT = 1

CAPTCHA_MAX_GY_SHIFT = 10

CAPTCHA_MIN_LINES = 1
CAPTCHA_MAX_LINES = 10

CAPTCHA_LINES_COLORS = (
    (0, 0, 0),
    (255, 255, 255),
    (255, 0, 0),
    (0, 255, 0),
    (0, 0, 255)


)

CAPTCHA_LINES_MIN_DY = 0
CAPTCHA_LINES_MAX_DY = 10
CAPTCHA_LINES_MAX_DX = 180
CAPTCHA_LINES_MIN_DX = 50
CAPTCHA_LINES_DY = 3
CAPTCHA_LINES_START_XPOS = 3
CAPTCHA_LINES_START_YPOS = 10

CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
BROKER_BACKEND = "djkombu.transport.DatabaseTransport"

LOCALE_PATHS = (
    os.path.join(PROJECT_ROOT, "locale"),
)
