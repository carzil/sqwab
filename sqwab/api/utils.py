#(c) Andreev Alexander 2012 (aka Carzil)
from functools import wraps
from django.http import HttpResponse
from simplejson import dumps as dump

def prepare_response(d):
    if d:
        return str(d).replace("'", '"')
    else:
        return str("{}")

def json_type():
    def json_typer(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            return HttpResponse(dump(func(*args, **kwargs)))
        return wrapper
    return json_typer

