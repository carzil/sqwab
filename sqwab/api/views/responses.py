#(c) Andreev Alexander 2012 (aka Carzil)

ERRORS_TEXT = {
    "pnf": "Post not found",
    "inp": "Invalid parameters",
    "ae": "Already exists",
    "nl": "Not liked",
    "ihm": "Invalid HTTP method",
    "onf": "Object not found",
    "np": "No permissions"
}

ERRORS_CODES = {
    "pnf": 101,
    "inp": 102,
    "ae": 103,
    "nl": 104,
    "ihm": 105,
    "np": 106,
    "onf": 404

}

def get_response_succ():
    response = {
        "type": "success",
        "code": 0
    }
    return response

def get_response_succa(d):
    response = {
        "type": "success",
        "code": 0
    }
    response.update(d)
    return response

def get_response_ihm():
    response = {
        "type": "error",
        "reason": ERRORS_TEXT["ihm"],
        "code": ERRORS_CODES["ihm"]
    }
    return response

def get_response_nl():
    response = {
        "type": "error",
        "reason": ERRORS_TEXT["nl"],
        "code": ERRORS_CODES["nl"]
    }
    return response

def get_response_pnf():
    response = {
        "type": "error",
        "reason": ERRORS_TEXT["pnf"],
        "code": ERRORS_CODES["pnf"]
    }
    return response

def get_response_inp():
    response = {
        "type": "error",
        "reason": ERRORS_TEXT["inp"],
        "code": ERRORS_CODES["inp"]
    }
    return response

def get_response_ae():
    response = {
        "type": "error",
        "reason": ERRORS_TEXT["ae"],
        "code": ERRORS_CODES["ae"]
    }
    return response

def get_response_onf():
    response = {
        "type": "error",
        "reason": ERRORS_TEXT["onf"],
        "code": ERRORS_CODES["onf"]
    }
    return response

def get_response_np():
    response = {
        "type": "error",
        "reason": ERRORS_TEXT["np"],
        "code": ERRORS_CODES["np"]
    }
    return response
