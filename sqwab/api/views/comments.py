#(c) Andreev Alexander 2012 (aka Carzil)
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.shortcuts import render
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt
from sqwab.api.utils import json_type
from sqwab.core.models.post import Post
from sqwab.core.forms.comments import CommentForm
from sqwab.core.models.comments import Comment
from sqwab.api.views.responses import *

class CommentMock(object):
    def __init__(self, author, text):
        self.author = author
        self.submit_time = datetime.now()
        self.text = text

@csrf_exempt
def comment_preview(request):
    comment = CommentMock(request.user, request.REQUEST.get("text", ""))
    return render(request, "blog/post/render_comment.html", {"comment": comment, "is_preview": True})

@json_type()
@csrf_exempt
def comment_write(request):
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            try:
                pid = form.cleaned_data["post"]
                post = Post.objects.get(id=pid)
            except (ValueError, ObjectDoesNotExist):
                return get_response_onf()
            try:
                cid = form.cleaned_data["parent"]
                if cid:
                    parent = Comment.objects.get(id=cid)
                    Comment.objects.create(
                        text=form.cleaned_data["comment"],
                        post=post,
                        author=request.user,
                        parent=parent
                    )
                else:
                    Comment.objects.create(
                        text=form.cleaned_data["comment"],
                        post=post,
                        author=request.user,
                    )
            except (ValueError, ObjectDoesNotExist):
                return get_response_onf()
        else:
            return get_response_inp()
    else:
        return get_response_inp()
    return get_response_succ()


























