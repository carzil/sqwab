# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from django.views.decorators.csrf import csrf_exempt
from simplejson import dumps as jdumps
from simplejson import loads as jload
from sqwab.api.utils import json_type
from sqwab.core.models.like import PostLike
from sqwab.core.models.post import Post
from sqwab.core.views.utils import format_likes
from sqwab.api.views.responses import *

@csrf_exempt
@login_required
def get_likes(request):
    if request.method == "POST":
        try:
            pid = int(request.REQUEST["pid"])
        except (ValueError, MultiValueDictKeyError):
            response = get_response_inp()
            return HttpResponse(jdumps(response))
        likes = PostLike.objects.filter(post__id=pid).count()
        return HttpResponse(jdumps(get_response_succa({
            "likes": likes,
            "likes_human": format_likes(likes)
        })))
    return HttpResponse(jdumps(get_response_ihm()))

@csrf_exempt
@login_required
@json_type()
def get_likes_many(request):
    pids = map(int, request.REQUEST.get("pids", "").split(","))
    likes_arr = []
    for i in pids:
        likes_arr.append(PostLike.objects.filter(post__id=i).count())
    response = get_response_succa({
        "likes": likes_arr,
        "likes_human": list(map(format_likes, likes_arr))
    })
    return response

@csrf_exempt
@login_required
def set_like(request):
    if request.method == "POST":
        try:
            pid = int(request.POST["pid"])
        except (ValueError, MultiValueDictKeyError):
            response = get_response_inp()
            return HttpResponse(jdumps(response))
        try:
            post = Post.objects.get(id=pid)
        except ObjectDoesNotExist:
            response = get_response_pnf()
            return HttpResponse(jdumps(response))
        like, is_created = PostLike.objects.get_or_create(user=request.user, post=post)
        if not is_created:
            response = get_response_ae()
            return HttpResponse(jdumps(response))
        return HttpResponse(jdumps(get_response_succ()))
    return HttpResponse(jdumps(get_response_ihm()))

@csrf_exempt
@login_required
def delete_like(request):
    if request.method == "POST":
        try:
            pid = int(request.POST["pid"])
        except (ValueError, MultiValueDictKeyError):
            response = get_response_inp()
            return HttpResponse(jdumps(response))
        try:
            post = Post.objects.get(id=pid)
        except ObjectDoesNotExist:
            response = get_response_pnf()
            return HttpResponse(jdumps(response))
        try:
            like = PostLike.objects.get(user=request.user, post=post)
        except ObjectDoesNotExist:
            response = get_response_nl()
            return HttpResponse(jdumps(response))
        like.delete()
        return HttpResponse(jdumps(get_response_succ()))
    return HttpResponse(jdumps(get_response_ihm()))
