from django.core.exceptions import ObjectDoesNotExist
from sqwab.api.utils import json_type
from sqwab.core.models.user import User
from django.http import HttpResponse
from sqwab.api.views.responses import *

@json_type()
def check_username(request):
    username = request.REQUEST.get("username", None)
    if username:
        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return get_response_succa({"exists": "false"})
        return get_response_succa({"exists": "true"})
    else:
        return get_response_inp()

@json_type()
def check_email(request):
    email = request.REQUEST.get("email", None)
    if email:
        try:
            User.objects.get(email=email)
        except ObjectDoesNotExist:
            return get_response_succa({"exists": "false"})
        return get_response_succa({"exists": "true"})
    else:
        return get_response_inp()
