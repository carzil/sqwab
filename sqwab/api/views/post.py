# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from simplejson import dumps as jdumps
from sqwab.api.utils import json_type
from sqwab.api.views.responses import *
from sqwab.core.models.blog import BlogMembership
from sqwab.core.models.post import Post
from postmarkup import create

@csrf_exempt
@login_required
def delete_post(request):
    if request.method == "POST":
        try:
            pid = int(request.POST["pid"])
        except (ValueError, ObjectDoesNotExist):
            return HttpResponse(jdumps(get_response_inp()))
    try:
        post = Post.objects.get(id=pid)
        blog = post.blog
        if request.user.is_staff:
            post.delete()
            return HttpResponse(jdumps(get_response_succ()))
        bsh = BlogMembership.objects.get(user=request.user, blog=blog)
    except ObjectDoesNotExist:
        return HttpResponse(jdumps(get_response_onf()))

    if bsh.can_edit_posts:
        post.delete()
        return HttpResponse(jdumps(get_response_succ()))
    else:
        return HttpResponse(jdumps(get_response_np()))

@json_type()
@csrf_exempt
@login_required
def preview(request):
    if request.method == "POST":
        parser = create(render_unknown_tags=True)
        text = request.REQUEST.get("text")
        if text:
            return get_response_succa({"text": parser(text)})
        else:
            return get_response_inp()
    else:
        return get_response_ihm()
