#(c) Andreev Alexander 2012 (aka Carzil)
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from simplejson import dumps as jdumps
from sqwab.core.models.blog import Blog, BlogMembership
from sqwab.api.views.responses import *

@csrf_exempt
def subscribe_blog(request):
    try:
        bid = int(request.REQUEST.get("bid", ""))
        blog = Blog.objects.get(id=bid)
    except (ValueError, ObjectDoesNotExist):
        return HttpResponse(jdumps(get_response_onf()))
    bmsh, is_created = BlogMembership.objects.get_or_create(user=request.user, blog=blog)
    if not is_created:
        return HttpResponse(jdumps(get_response_ae()))
    return HttpResponse(jdumps(get_response_succ()))

@csrf_exempt
def unsubscribe_blog(request):
    try:
        bid = int(request.REQUEST.get("bid", ""))
        blog = Blog.objects.get(id=bid)
    except (ValueError, ObjectDoesNotExist):
        return HttpResponse(jdumps(get_response_onf()))
    try:
        bmsh = BlogMembership.objects.get(user=request.user, blog=blog)
    except ObjectDoesNotExist:
        return HttpResponse(jdumps(get_response_onf()))
    if not bmsh.is_creator:
        bmsh.delete()
    return HttpResponse(jdumps(get_response_succ()))




