#(c) Andreev Alexander 2012 (aka Carzil)
from django.http import HttpResponse
from sqwab.api.utils import prepare_response
from sqwab.core.models.user import User

def get_main(request):
    return HttpResponse(prepare_response({}))