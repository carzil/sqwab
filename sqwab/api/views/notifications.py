#(c) Andreev Alexander 2012 (aka Carzil)
from django.http import HttpResponse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import simplejson

@csrf_exempt
def get_all(request):
    m = messages.get_messages(request)
    nots = {"notifications": []}
    for i in m:
        nots["notifications"].append(i.message)
    return HttpResponse(simplejson.dumps(nots))

@csrf_exempt
def add(request):
    messages.add_message(request, messages.INFO, request.GET["n"])
    return HttpResponse(request.GET["n"])