#(c) Andreev Alexander 2012 (aka Carzil)
from django.conf.urls.defaults import patterns, url
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns("sqwab.api.views",
    url(r"^$", "core.get_main"),

    url(r"^user/check_username$", "user.check_username"),
    url(r"^user/check_email$", "user.check_email"),

    url(r"^comments/preview/$", "comments.comment_preview"),
    url(r"^comments/write/$", "comments.comment_write"),


    url(r"^post/likes/$", "likes.get_likes"),
    url(r"^post/likes_many/$", "likes.get_likes_many"),
    url(r"^post/like/$", "likes.set_like"),
    url(r"^post/unlike/$", "likes.delete_like"),
    url(r"^post/delete/$", "post.delete_post"),
    url(r"^post/preview/$", "post.preview"),

    url(r"^blog/subscribe/$", "blog.subscribe_blog"),
    url(r"^blog/unsubscribe/$", "blog.unsubscribe_blog"),

)

