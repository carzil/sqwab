#(c) Andreev Alexander 2012 (aka Carzil)
from django.shortcuts import render
from haystack.query import SearchQuerySet

def search(request):
    new_search = True
    results = []
    query = request.GET.get("q", "")
    if request.GET.has_key("q"):
        new_search = False
        if query:
            results = SearchQuerySet().filter(content=request.GET.get("q", ""))
    return render(request, "search/search.html", {
        "result": results,
        "nsearch": new_search,
        "query": query
    })
