#!/usr/bin/python
from django.shortcuts import render, redirect
from urllib import unquote_plus as up
from sqwab.core.forms.login import AuthenticationForm
from sqwab.core.models.blog import BlogMembership
from sqwab.core.models.post import Post

def get_feed(request):
    bmshs = BlogMembership.objects.filter(user=request.user)
    posts = Post.objects.filter(blog__in=(i.blog for i in bmshs))
    posts = posts.order_by("-publication_date")
    return render(request, "core/feed.html", {"post_feed": posts})

def get_main(request):
    if request.user.is_authenticated():
        return get_feed(request)
    else:
        form = AuthenticationForm()
        return render(request, "core/login.html", {"form_auth": form})

def get_about(request):
    return render(request, "core/about.html")

def away(request):
    if request.method == "GET":
        try:
            return redirect(up(request.GET["to"]))
        except:
            pass
    return redirect("/")
