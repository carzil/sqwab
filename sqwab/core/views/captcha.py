# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
import Image, ImageFont, ImageDraw
from django.conf import settings
import random
from hashlib import md5
from sqwab.core.models.captcha import Captcha
from django.http import HttpResponse
from sqwab.core.views.utils import get_random_string

def save_captcha(text):
    text = text.encode("utf-8")
    m = md5()
    m.update(text)
    key = m.hexdigest()
    Captcha.objects.create(
        solution=key
    )

def generate_lines(cdraw, size, cnt):
    lypos = settings.CAPTCHA_LINES_START_YPOS
    lxpos = settings.CAPTCHA_LINES_START_XPOS
    for i in range(cnt):
        cdraw.line([(lxpos, lypos),
            (lxpos + random.randrange(
                settings.CAPTCHA_LINES_MIN_DY,
                settings.CAPTCHA_LINES_MAX_DY
            ), lypos + random.randrange(
            settings.CAPTCHA_LINES_MIN_DY, settings.CAPTCHA_LINES_MAX_DY))],
            fill=random.choice(settings.CAPTCHA_LINES_COLORS))
        lypos += settings.CAPTCHA_LINES_DY


def plazma(width, height):
    img = Image.new("RGB", (width, height))
    pix = img.load()

    for xy in [(0,0), (width-1, 0), (0, height-1), (width-1, height-1)]:
        rgb = []
        for i in range(3):
            rgb.append(int(random.random()*256))
        pix[xy[0],xy[1]] = (rgb[0], rgb[1], rgb[2])

    plazmaRec(pix, 0, 0, width-1, height-1)
    return img

def plazmaRec(pix, x1, y1, x2, y2):
    if (abs(x1 - x2) <= 1) and (abs(y1 - y2) <= 1):
        return

    rgb = []
    for i in range(3):
        rgb.append((pix[x1, y1][i] + pix[x1, y2][i])/2)
        rgb.append((pix[x2, y1][i] + pix[x2, y2][i])/2)
        rgb.append((pix[x1, y1][i] + pix[x2, y1][i])/2)
        rgb.append((pix[x1, y2][i] + pix[x2, y2][i])/2)

        tmp = (pix[x1, y1][i] + pix[x1, y2][i] + pix[x2, y1][i] + pix[x2, y2][i])/4
        diagonal =  ((x1-x2)**2 + (y1-y2)**2)**0.5
        while True:
            delta = int ( ((random.random() - 0.5)/100 * min(100, diagonal))*255 )
            if (tmp + delta >= 0) and (tmp + delta <= 255):
                tmp += delta
                break
        rgb.append(tmp)

    pix[x1, (y1 + y2)/2] = (rgb[0], rgb[5], rgb[10])
    pix[x2, (y1 + y2)/2]= (rgb[1], rgb[6], rgb[11])
    pix[(x1 + x2)/2, y1] = (rgb[2], rgb[7], rgb[12])
    pix[(x1 + x2)/2, y2] = (rgb[3], rgb[8], rgb[13])
    pix[(x1 + x2)/2, (y1 + y2)/2] = (rgb[4], rgb[9], rgb[14])

    plazmaRec(pix, x1, y1, (x1+x2)/2, (y1+y2)/2)
    plazmaRec(pix, (x1+x2)/2, y1, x2, (y1+y2)/2)
    plazmaRec(pix, x1, (y1+y2)/2, (x1+x2)/2, y2)
    plazmaRec(pix, (x1+x2)/2, (y1+y2)/2, x2, y2)


def make_bg(cdraw, captcha):
    captcha = plazma(captcha.size[0], captcha.size[1])
    return captcha


def generate_captcha_text():
    return get_random_string(settings.CAPTCHA_TEXT_LENGTH, ac="abdefghjknpqrstuvxyz23458")

def make_char(captcha, size, char, font, xpos, ypos):
    fgimg = Image.new(
        "RGBA",
        size,
        random.choice(settings.CAPTCHA_TEXT_COLORS)
    )
    csize = font.getsize("%s" % char)
    charimg = Image.new("L", csize)
    imgdraw = ImageDraw.Draw(charimg)

    imgdraw.text(
        (0, 0),
        "%s" % char,
        fill="#ffffff",
        font=font)

    if settings.CAPTCHA_TEXT_MIN_ROTATION or settings.CAPTCHA_TEXT_MAX_ROTATION:
        charimg = charimg.rotate(
            random.randrange(
                settings.CAPTCHA_TEXT_MIN_ROTATION,
                settings.CAPTCHA_TEXT_MAX_ROTATION
            ),
            expand=0,
            resample=Image.BICUBIC
        )

    mask = Image.new("L", size)
    mask.paste(charimg, (xpos, ypos, xpos + charimg.size[0], ypos + charimg.size[1]))

    captcha = Image.composite(fgimg, captcha, mask)

    return captcha, csize

def get_captcha(request):
    response = HttpResponse(mimetype="image/png")

    ctext = generate_captcha_text()

    color = random.choice(settings.CAPTCHA_BGCOLORS)
    size = settings.CAPTCHA_SIZE
    font = ImageFont.truetype(random.choice(settings.CAPTCHA_FONTS), settings.CAPTCHA_TEXT_SIZE)

    p = [False for i in range(settings.CAPTCHA_Y_PROBABILITY_SHIFT)]
    p.append(True)

    captcha = Image.new("RGB", size, color)
    cdraw = ImageDraw.Draw(captcha)

    captcha = make_bg(cdraw, captcha)

    lines_cnt = random.randrange(settings.CAPTCHA_MIN_LINES, settings.CAPTCHA_MAX_LINES)
    first_step_lines = random.randrange(settings.CAPTCHA_MIN_LINES, settings.CAPTCHA_MAX_LINES)

    generate_lines(cdraw, size, first_step_lines)

    xpos = 1
    ypos = 6

    for char in ctext:
        captcha, csize = make_char(captcha, size, char, font, xpos, ypos)

        if settings.CAPTCHA_MIN_LETTER_DISTANCE and settings.CAPTCHA_MAX_LETTER_DISTANCE:
            xpos += csize[0] + random.randrange(
                settings.CAPTCHA_MIN_LETTER_DISTANCE,
                settings.CAPTCHA_MAX_LETTER_DISTANCE
            )
        elif settings.CAPTCHA_MIN_LETTER_DISTANCE == settings.CAPTCHA_MAX_LETTER_DISTANCE:
            xpos += csize[0] + settings.CAPTCHA_MIN_LETTER_DISTANCE

        if random.choice(p):
            dy = random.randrange(
                settings.CAPTCHA_MIN_Y_SHIFT,
                settings.CAPTCHA_MAX_Y_SHIFT
            )
            if (ypos + dy) < settings.CAPTCHA_MAX_GY_SHIFT:
                ypos += dy

    generate_lines(cdraw, size, lines_cnt - first_step_lines)

    captcha.save(response, "PNG")
    save_captcha(ctext)
    return response
