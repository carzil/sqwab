# -*- coding=utf-8 -*-
import random
from django.core.exceptions import ObjectDoesNotExist
from sqwab.core.models.pm import *
from sqwab.core.models.user import UserProfile, get_uphoto_path
from django.utils.translation import ugettext as _
from django.utils.translation import ungettext

def cut_text(text, flen):
    text = unicode(text)
    if len(text) > (flen - 3):
        return text[:flen - 3] + u"..."
    else:
        return text

def check_for_params(params, request):
    for i in params:
        if request.get(i, True):
            return False

def pm_context_processor(request):
    c = PMToUser.objects.filter(pm__is_read=False).count()
    d = {"pm_unread": c, "site_name": "Sqwab",}
    if not request.user.is_anonymous():
        try:
            _tmp = UserProfile.objects.get(user=request.user)
            path = get_uphoto_path(_tmp, _tmp.photo.name)
            d["user_photo_path"] = path
        except ObjectDoesNotExist:
            d["user_photo_path"] = None
    return d

def get_random_string(length=10, ac="abcdefghijklmnopqrstuvwxyz0123456789"):
    return "".join((random.choice(ac) for i in range(length)))

def get_random_filename(extension=None, length=20):
    if extension:
        return ".".join((get_random_string(length), extension))
    return get_random_string(length)

def get_ending(number, endings):
    number %= 100
    if 11 <= number <= 19:
        ending = endings[2]
    else:
        i = number % 10
        if i == 1:
            ending = endings[0]
        elif i == 2 or i == 3 or i == 4:
            ending = endings[1]
        else:
            ending = endings[2]
    return ending

def format_likes(number):
    if not number:
        return _("nobody have liked it yet")
    else:
        return ungettext(u"%(count)d like", u"%(count)d likes", number) % {"count": number}