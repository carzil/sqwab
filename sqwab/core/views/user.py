#!/usr/bin/python
# -*- coding=utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.shortcuts import render, redirect
from django.contrib.auth import logout
from django.utils.datastructures import MultiValueDictKeyError
from sqwab.core.models.blog import BlogMembership
from sqwab.core.models.user import User, UserProfile
from sqwab.core.forms.login import AuthenticationForm
from sqwab.core.forms.registration import RegistrationForm
from sqwab.core.forms.profile import ProfileEditForm
from sqwab.core.views.utils import get_random_filename
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.translation import ugettext as _

@login_required
def get_profile(request, id=False):
    if id:
        try:
            u = User.objects.get(id=id)
        except ObjectDoesNotExist:
            u = request.user
    else:
        u = request.user
    blogs = [i.blog for i in BlogMembership.objects.filter(user=u)]
    return render(request, "user/profile.html", {"puser": u, "blog_list": blogs})

def log_in(request):
    if request.method == "POST":
        form = AuthenticationForm(request.POST)
        if form.is_valid():
            return form.login(request, request.POST.get("next", "/"))
        else:
            return render(request, "core/login.html", {"form_auth": form})
    else:
        form = AuthenticationForm()
    return render(request, "core/login.html", {"form_auth": form, "next_url": request.GET.get("next", "/")})

@login_required
def log_out(request):
    logout(request)
    return redirect("/")

def registration(request):
    if request.method == "POST":
        form_reg = RegistrationForm(request.POST)
        if form_reg.is_valid():
            user = form_reg.save()
            form_reg.login_user(request)
            UserProfile.objects.create(user=user)
            return redirect("/")
    else:
        form_reg = RegistrationForm()
    return render(request, "core/registration.html", {"form_reg": form_reg})

def handle_new_user_photo(request, photo, profile, is_created):
    if not is_created:
        profile.photo.delete()
    profile.photo.save(".".join([get_random_filename(), photo.name.split(".")[-1]]), photo)
    print profile.photo

@login_required
def edit_profile(request):
    profile, is_created = UserProfile.objects.get_or_create(user=request.user)
    if request.method == "GET":
        initial = {
            "first_name": request.user.first_name,
            "last_name": request.user.last_name,
            "birthday": profile.birthday
        }
        form = ProfileEditForm(initial=initial)
    else:
        form = ProfileEditForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                f = request.FILES["photo"]
                handle_new_user_photo(request, f, profile, is_created)
            except MultiValueDictKeyError:
                pass
            request.user.first_name = form.cleaned_data["first_name"]
            request.user.last_name = form.cleaned_data["last_name"]
            profile.birthday = form.cleaned_data["birthday"]
            request.user.save()
            profile.save()
            messages.info(request, "|".join([
                _("Changes saved"),
                _("Your profile successfully updated.")
            ]), extra_tags="success")
            return redirect("/profile/")
    return render(request, "user/profile_edit.html", {"form_pu": form})
