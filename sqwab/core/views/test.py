# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
from django.http import HttpResponse
from django.contrib import messages

def get_test(request):
    messages.info(
        request,
        "|".join([
            request.REQUEST.get("head", ""),
            request.REQUEST.get("msg", "")
        ]),
        extra_tags=request.REQUEST.get("tags", ""),
    )
    return HttpResponse("test")