#(c) Andreev Alexander 2012 (aka Carzil)
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.translation import check_for_language, activate, get_language_from_request
from sqwab.core.forms.usettings import SettingsForm

def set_lang(request, lang):
    lang_code = lang
    if lang_code and check_for_language(lang_code):
        if hasattr(request, 'session'):
            request.session['django_language'] = lang_code
        activate(lang_code)

@login_required
def usettings(request):
    if request.method == "POST":
        form = SettingsForm(request.POST)
        if form.is_valid():
            lang = form.cleaned_data["lang"]
            set_lang(request, lang)
    else:
        initial = {
            "lang": get_language_from_request(request)
        }
        form = SettingsForm(initial=initial)
    return render(request, "core/settings.html", {"form": form})
