#(c) Andreev Alexander 2012 (aka Carzil)
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.utils.datastructures import MultiValueDictKeyError
from sqwab.core.forms.comments import CommentForm
from sqwab.core.models.comments import Comment
from sqwab.core.models.blog import Blog, BlogMembership
from sqwab.core.models.post import Post
from sqwab.core.forms.post import PostAddForm, LinkAddForm, ImageAddForm, ImageEditForm
from sqwab.core.stats import add_uba
from sqwab.core.views.utils import get_random_filename

@login_required
def get_blog(request, bid=None):
    if bid:
        try:
            bid = int(bid)
        except (ValueError, TypeError):
            return render(request, "blog/blog_fail.html")

        try:
            blog = Blog.objects.get(id=bid)
            owner = BlogMembership.objects.get(blog=blog, is_creator=True).user
        except ObjectDoesNotExist:
            return render(request, "blog/blog_fail.html")

        try:
            bmsh = BlogMembership.objects.get(blog=blog, user=request.user)
        except ObjectDoesNotExist:
            bmsh = None
    else:
        bmsh = BlogMembership.objects.get(blog__is_main=True, user=request.user, is_creator=True)
        blog = bmsh.blog
        owner = request.user
    add_uba(request.user, blog)
    posts = Post.objects.filter(blog=blog).order_by("-publication_date")
    return render(request, "blog/blog.html", {"blog": blog, "posts": posts, "owner": owner, "bmsh": bmsh})

def get_comment_parent(cid):
    try:
        cid = int(cid)
        parent = Comment.objects.get(id=cid)
    except (ValueError, ObjectDoesNotExist, TypeError):
        return None
    return parent

@login_required
def get_post(request, pid=None):
    try:
        pid = int(pid)
        post = Post.objects.get(id=pid)
    except (ValueError, ObjectDoesNotExist):
        raise Http404()
    if request.method == "POST":
        cform = CommentForm(request.REQUEST)
        if cform.is_valid():
            Comment.objects.create(
                post=post,
                author=request.user,
                text=cform.cleaned_data["comment"],
                parent=get_comment_parent(cform.cleaned_data["parent"])
            )
    else:
        cform = CommentForm(initial={"parent": None, "post": post.id})
    comments = Comment.objects.filter(post=post)
    return render(request, "blog/post/post.html", {"post": post, "comments": comments, "comment_form": cform})

def handle_new_image(request, file, post):
    post.image.save(".".join([get_random_filename(), file.name.split(".")[-1]]), file)
    print post.image

@login_required
def add_image(request, bid):
    try:
        bid = int(bid)
        blog = Blog.objects.get(id=bid)
    except (ValueError, ObjectDoesNotExist):
        return Http404()
    if request.method == "POST":
        form = ImageAddForm(request.POST, request.FILES)
        if form.is_valid():
            post = Post(
                type="i",
                title=form.cleaned_data["title"],
                text=form.cleaned_data["text"],
                tags=form.cleaned_data["tags"],
                author=request.user,
                blog=blog,
                is_posted=True,
            )
            try:
                f = request.FILES["image"]
                handle_new_image(request, f, post)
            except MultiValueDictKeyError:
                pass
            post.save()
            return redirect(post.get_absolute_url())

    else:
        form = ImageAddForm()
    return render(request, "blog/add/post.html", {"form_add": form, "blog": blog})

@login_required
def add_post(request, bid):
    try:
        bid = int(bid)
        blog = Blog.objects.get(id=bid)
    except (ValueError, ObjectDoesNotExist):
        return Http404()
    if request.method == "POST":
        form = PostAddForm(request.POST)
        if form.is_valid():
            post = Post(
                type="p",
                title=form.cleaned_data["title"],
                text=form.cleaned_data["text"],
                tags=form.cleaned_data["tags"],
                author=request.user,
                blog=blog,
                is_posted=True,
            )
            post.save()
            return redirect(post.get_absolute_url())

    else:
        form = PostAddForm()
    return render(request, "blog/add/post.html", {"form_add": form, "blog": blog})

@login_required
def add_link(request, bid):
    try:
        bid = int(bid)
        blog = Blog.objects.get(id=bid)
    except (ValueError, ObjectDoesNotExist):
        return Http404()
    if request.method == "POST":
        form = LinkAddForm(request.POST)
        if form.is_valid():
            post = Post(
                type="l",
                title=form.cleaned_data["title"],
                text=form.cleaned_data["text"],
                tags=form.cleaned_data["tags"],
                link=form.cleaned_data["link"],
                author=request.user,
                blog=blog,
                is_posted=True,
            )
            post.save()
            return redirect(post.get_absolute_url())

    else:
        form = LinkAddForm()
    return render(request, "blog/add/post.html", {"form_add": form, "blog": blog})

def add_link_default(request):
    blog = BlogMembership.objects.get(is_creator=True, blog__is_main=True, user=request.user)
    return add_link(request, blog.blog.id)

def add_post_default(request):
    blog = BlogMembership.objects.get(is_creator=True, blog__is_main=True, user=request.user)
    return add_post(request, blog.blog.id)

def add_image_default(request):
    blog = BlogMembership.objects.get(is_creator=True, blog__is_main=True, user=request.user)
    return add_image(request, blog.blog.id)

@login_required
def edit_image(request, pid):
    try:
        pid = int(pid)
        post = Post.objects.get(id=pid, type="i")
    except (ValueError, ObjectDoesNotExist):
        return Http404()
    if request.method == "POST":
        form = ImageEditForm(request.POST, request.FILES)
        if form.is_valid():
            post.title=form.cleaned_data["title"]
            post.text=form.cleaned_data["text"]
            post.tags=form.cleaned_data["tags"]

            try:
                f = request.FILES["image"]
                if f:
                    handle_new_image(request, f, post)
            except MultiValueDictKeyError:
                pass
            post.save()
            return redirect(post.get_absolute_url())

    else:
        form = ImageEditForm(initial={"text": post.text,
                                    "title": post.title,
                                    "tags": post.tags,
                                    "blog": post.blog,
        })
    return render(request, "blog/add/post.html", {"form_add": form, "update": True, "post": post})

@login_required
def edit_post(request, pid):
    try:
        pid = int(pid)
        post = Post.objects.get(id=pid, type="p")
    except (ValueError, ObjectDoesNotExist):
        return Http404()
    if request.method == "POST":
        form = PostAddForm(request.POST)
        if form.is_valid():
            post.type="p"
            post.title=form.cleaned_data["title"]
            post.text=form.cleaned_data["text"]
            post.tags=form.cleaned_data["tags"]
            post.author=request.user
            post.save()
            return redirect(post.get_absolute_url())

    else:
        form = PostAddForm(initial={"text": post.text,
            "title": post.title,
            "tags": post.tags,
            "blog": post.blog
        })
    return render(request, "blog/add/post.html", {"form_add": form, "update": True, "post": post})

@login_required
def edit_link(request, pid):
    try:
        pid = int(pid)
        post = Post.objects.get(id=pid, type="l")
    except (ValueError, ObjectDoesNotExist):
        return Http404()
    if request.method == "POST":
        form = LinkAddForm(request.POST)
        if form.is_valid():
            post.type = "l"
            post.title = form.cleaned_data["title"]
            post.link = form.cleaned_data["link"]
            post.text = form.cleaned_data["text"]
            post.tags = form.cleaned_data["tags"]
            post.author = request.user
            post.save()
            return redirect(post.get_absolute_url())

    else:
        form = LinkAddForm(initial={
            "text": post.text,
            "title": post.title,
            "link": post.link,
            "tags": post.tags,
            "blog": post.blog
        })
    return render(request, "blog/add/post.html", {"form_add": form, "update": True, "post": post})

def edit_blog(request, bid=None):
    raise Http404()
