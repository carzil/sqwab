#(c) Andreev Alexander 2012 (aka Carzil)
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from sqwab.core.models.user import User
from sqwab.core.models.friends import Friendship

@login_required
def get_friends(request, uid=None):
    if uid:
        try:
            user = User.objects.get(id=uid)
        except ObjectDoesNotExist:
            user = request.user
    else:
        user = request.user
    friends = [i.user2 for i in Friendship.objects.filter(user1=user, is_accepted=True)]
    friends2 = [i.user1 for i in Friendship.objects.filter(user2=user, is_accepted=True)]
    friends.extend(friends2)

    return render(request, "friends/friend_list.html", {"friends_list": friends})




