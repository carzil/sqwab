#(c) Andreev Alexander 2012 (aka Carzil)
from haystack import indexes
from sqwab.core.models.post import Post


class PostIndex(indexes.RealTimeSearchIndex, indexes.Indexable, indexes.ModelSearchIndex):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr="title")
    type = indexes.CharField(model_attr="type")
    post_text = indexes.CharField(model_attr="text")
    author = indexes.CharField(model_attr="author")
    author_id = indexes.IntegerField(model_attr="author__id")
    publication_date = indexes.DateTimeField(model_attr="publication_date")
    tags = indexes.CharField(model_attr="tags")
    id = indexes.IntegerField(model_attr="id")
    link = indexes.CharField(model_attr="link")

    def get_model(self):
        return Post

    def index_queryset(self):
        return self.get_model().objects.all()

    class Meta:
        model = Post
