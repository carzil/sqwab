from django.contrib import admin
from sqwab.core.models.blog import *
from sqwab.core.models.post import *
from sqwab.core.models.user import *
from sqwab.core.models.pm import *
from sqwab.core.models.friends import *
from sqwab.core.models.captcha import *
from sqwab.core.models.comments import *
from sqwab.core.models.like import PostLike
from sqwab.core.models.stats import UserBlogActivity

admin.site.register([Blog,
                     BlogMembership,
                     Post,
                     UserProfile,
                     PM,
                     PMToUser,
                     Friendship,
                     Captcha,
                     Comment,
                     PostLike,
                     UserBlogActivity

])