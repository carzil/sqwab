function reply(cid) {
    var rform;
    if (cid != 0) {
        $(".write_tlc_link").show();
        rform = $("#comment" + cid + " .reply_form").show();
    } else {
        $(".write_tlc_link").hide();
        rform = $(".blog")
    }
    var form = $("#comment_form_container");
    $(".preview_place", form).empty();
    rform.append(form);
    $("input#id_parent", form).attr({value: cid});
    $("textarea#id_comment", rform).focus();
}

$(document).ready(function() {
    $("#preview_link").click(function() {
        var form = $(this).parent().parent();
        var preview_place = $(".preview_place", form);
        var text = encodeURIComponent($("#id_comment", form).val());
        var parent = form.parent().parent();
        var request = $.ajax({
            type: "POST",
            url: "/json/comments/preview/",
            data: "text=" + text,
            cache: false
        });
        request.success(function(data) {
            if (parent.attr("class") == "reply_form") preview_place.css("margin-left", "30px");
            preview_place.show().html(data);
        });
        return false;
    });
});

function comment_write(form) {
    $(form).ajaxSubmit({
        success: function(json){
            if (json.type != "error") {
                location.reload();
            } else {
                show_system_error(json);
            }
        }
    });

}
