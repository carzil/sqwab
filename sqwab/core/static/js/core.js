$(document).ready(function() {
    $("#to_top").hide();
    $("#to_top *").click(function() {
        $.scrollTo({top: 0, left: 0}, 500)
    });
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $("#to_top").fadeIn(500);
        } else {
            $("#to_top").fadeOut(500);
        }
    })
});

function show_system_error(json) {
    $.jGrowl(gettext("Error code: ") + json.code , {
        position: "bottom-right",
        theme: "error",
        header: gettext("Whoops! There is a server error :(")
    });
}
