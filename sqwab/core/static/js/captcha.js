$(document).ready(function() {
    $("a.captcha_refresh").click(function() {
        var link = $(this).text(gettext("Show new code"));
        $(this).prev().children("img")
            .attr({src: "/captcha/?" + new Date().getTime()})
            .load(function() {
                link.text(gettext("Show new code"));
            });
        $(this).text(gettext("Loading..."));
        return false;
    })
});