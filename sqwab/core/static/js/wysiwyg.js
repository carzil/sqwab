(function(window, $) {
    var SWYG = window.SWYG = function(){
        return {
            init: function(textarea) {
                this.textarea = $(textarea);
            },

            _insert: function(opener, closer) {
                this.textarea.focus();
                var tarea = this.textarea.get(0);
                var cursorPos = this.getCursor(tarea);
                var txt_pre = this.textarea.val().substring(0, cursorPos.start);
                var txt_sel = this.textarea.val().substring(cursorPos.start, cursorPos.end);
                var txt_aft = this.textarea.val().substring(cursorPos.end);
                this.textarea.val(txt_pre + opener + txt_sel + closer + txt_aft);
                return false;
            },

            getCursor: function(input){
                var result = {start: 0, end: 0};
                if (input.setSelectionRange){
                    result.start = input.selectionStart;
                    result.end = input.selectionEnd;
                } else if (!document.selection) {
                    return false;
                } else if (document.selection && document.selection.createRange) {
                    var range = document.selection.createRange();
                    var stored_range = range.duplicate();
                    stored_range.moveToElementText(input);
                    stored_range.setEndPoint('EndToEnd', range);
                    result.start = stored_range.text.length - range.text.length;
                    result.end = result.start + range.text.length;
                }
                return result;
            },

            insertTag: function(tagName, parameter) {
                if (parameter) return this._insert("[" + tagName + "=" + parameter + "]", "[/" + tagName + "]");
                return this._insert("[" + tagName + "]", "[/" + tagName + "]");
            },

            insertImage: function() {
                var img_src = prompt(gettext("Enter address of picture:"));
                if (img_src) return this._insert("[img]" + img_src + "[/img]", "");

                return false;
            },

            insertLink: function() {
                var link_href = prompt(gettext("Enter address of link:"));
                if (link_href) return this.insertTag("link", link_href);

            }
        }
    }()

})(window, jQuery);