function set_likes(pid, likes_human) {
    $("#post" + pid + " .footer span.likes_count")
        .text(likes_human + " |")
        .show(500);
}

function get_likes(id) {
    var request = $.ajax({
        type: "POST",
        url: "/json/post/likes/",
        data: "pid=" + id,
        cache: false
    });
    request.success(function(data) {
        var json = $.parseJSON(data);
        if (json.type != "error") {
            set_likes(id, json.likes_human);
        }
    });
}

function DeletePost(id) {
    if (confirm(gettext("This action can not be canceled. Continue?"))) {
        var request = $.ajax({
            type: "POST",
            url: "/json/post/delete/",
            data: "pid=" + id
        });
        request.success(function(data) {
            var json = $.parseJSON(data);
            if (json.type != "error") {
                $("#post" + id)
                    .slideUp(function(){$(this).remove()});
            } else {
                $.jGrowl(gettext("Error code: ") + json.code , {
                    position: "bottom-right",
                    theme: "error",
                    header: gettext("Whoops! There is a server error :(")});
            }
        });
        request.fail(function(a, b) {
            $.jGrowl(gettext("Whoops! There is a server error :("), {position: "bottom-right", theme: "error"});
        });
    }
}

function LikePost(id) {
    var request = $.ajax({
        type: "POST",
        url: "/json/post/like/",
        data: "pid=" + id
    });
    request.success(function(data) {
        var json = $.parseJSON(data);
        if (json.type != "error") {
            $("#post" + id + " .actions .like_link")
                .text(gettext("unlike"))
                .attr({onclick: "UnlikePost(" + id + "); return false;"})
                .removeClass("like_link")
                .addClass("unlike_link");
            get_likes(id);
        } else {
            $.jGrowl(gettext("Error code: ") + json.code , {
                position: "bottom-right",
                theme: "error",
                header: gettext("Whoops! There is a server error :(")});
        }
    });
    request.fail(function(a, b) {
        $.jGrowl(gettext("Whoops! There is a server error :("), {position: "bottom-right", theme: "error"});
    });
}

function UnlikePost(id) {
    var request = $.ajax({
        type: "POST",
        url: "/json/post/unlike/",
        data: "pid=" + id
    });
    request.success(function(data) {
        var json = $.parseJSON(data);
        if (json.type != "error") {
            $("#post" + id + " .actions .unlike_link")
                .text(gettext("like"))
                .attr({onclick: "LikePost(" + id + "); return false;"})
                .removeClass("unlike_link")
                .addClass("like_link");
            get_likes(id);
        } else {
            $.jGrowl(gettext("Error code: ") + json.code , {
                position: "bottom-right",
                theme: "error",
                header: gettext("Whoops! There is a server error :(")});
        }
    });
    request.fail(function(a, b) {
        $.jGrowl(gettext("Whoops! There is a server error :("), {position: "bottom-right", theme: "error"});
    });
}

$(document).ready(function() {
    $("button#preview_button").click(function() {
        var form = $(this).parent().parent();
        var preview_place = $(".preview_place", form);
        var text = encodeURIComponent($("#id_text", form).val());
        var request = $.ajax({
            type: "POST",
            url: "/json/post/preview/",
            data: "text=" + text,
            dataType: "json",
            cache: false
        });
        request.success(function(data) {
            preview_place.show().html(data.text);
            $.scrollTo(preview_place, 500);
            $("pre code", preview_place).each(function(i, e) {hljs.highlightBlock(e)});
        });
        return false;
    })
});