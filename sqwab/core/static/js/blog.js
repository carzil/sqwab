function SubscribeBlog(bid, link) {
    var request = $.ajax({
        type: "POST",
        url: "/json/blog/subscribe/",
        data: "bid=" + bid,
        cache: false
    });
    request.success(function(data) {
        var json = $.parseJSON(data);
        if (json.type != "error") {
            $(link)
                .text(gettext("unsibscribe"))
                .attr({onclick: "UnsubscribeBlog(" + bid + ", this); return false;"})
                .removeClass("subscribe_link")
                .addClass("unsubscribe_link");
            $.jGrowl(gettext("Your are now subscribed to this blog."), {
                position: "bottom-right",
                theme: "success"
            });
        } else {
            $.jGrowl(gettext("Error code: ") + json.code , {
                position: "bottom-right",
                theme: "error",
                header: gettext("Whoops! There is a server error :(")});
        }
        });
}

function UnsubscribeBlog(bid, link) {
    var request = $.ajax({
        type: "POST",
        url: "/json/blog/unsubscribe/",
        data: "bid=" + bid,
        cache: false
    });
    request.success(function(data) {
        var json = $.parseJSON(data);
        if (json.type != "error") {
            $(link)
                .text(gettext("subscribe"))
                .attr({onclick: "SubscribeBlog(" + bid + ", this); return false;"})
                .removeClass("unsubscribe_link")
                .addClass("subscribe_link");
            $.jGrowl(gettext("Now, you're unsubscribed from this blog"), {
                position: "bottom-right",
                theme: "orange"
            });
        } else {
            $.jGrowl(gettext("Error code: ") + json.code , {
                position: "bottom-right",
                theme: "error",
                header: gettext("Whoops! There is a server error :(")});
        }
    });
}