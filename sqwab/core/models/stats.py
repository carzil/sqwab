from django.db import models
from sqwab.core.models.user import User
from sqwab.core.models.blog import Blog
from datetime import datetime

class UserBlogActivity(models.Model):
    user = models.ForeignKey(User)
    blog = models.ForeignKey(Blog)
    date = models.DateField(default=datetime.now())
    cnt = models.BigIntegerField(default=1)

    class Meta:
        app_label = "core"

    def __unicode__(self):
        return " - ".join([unicode(self.user), unicode(self.blog), unicode(self.date), unicode(self.cnt)])