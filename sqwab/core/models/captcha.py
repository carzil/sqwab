#(c) Andreev Alexander 2012 (aka Carzil)
from django.db import models

class Captcha(models.Model):
    solution = models.CharField(max_length=32)
    generation_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = "core"

    def __unicode__(self):
        return " - ".join([unicode(self.solution), unicode(self.generation_time)])
