# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

def get_uphoto_path(instance, filename):
    uname = unicode(instance.user.username)
    return u"uploads/%s/avatar/%s" % (uname, filename)


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    photo = models.ImageField(null=False, blank=True, upload_to=get_uphoto_path)
    birthday = models.DateField(null=False, blank=True)
    about = models.TextField(blank=True)

    class Meta:
        app_label = "core"

class UserSettings(models.Model):
    user = models.ForeignKey(User)
    language = models.CharField(choices=settings.LANGUAGES, max_length=3)

    class Meta:
        app_label = "core"
