# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
from django.db import models
from sqwab.core.models.user import User
from django.utils.translation import ugettext_lazy as _

class Blog(models.Model):
    name = models.CharField(max_length=50, verbose_name=_("Blog name"))
    is_private = models.BooleanField(verbose_name=_("Is private?"))
    is_collective = models.BooleanField(verbose_name=_("Is collective?"))
    is_main = models.BooleanField(verbose_name=_("Is main?"))
    readers = models.ManyToManyField(User, through="BlogMembership")

    class Meta:
        app_label = "core"

    def __unicode__(self):
        return unicode(self.name)

class BlogMembership(models.Model):
    user = models.ForeignKey(User, verbose_name=_("User"))
    blog = models.ForeignKey(Blog, verbose_name=_("Blog"))
    is_creator = models.BooleanField(verbose_name=_("Is creator?"))
    is_admin = models.BooleanField(verbose_name=_("Is admin?"))
    is_banned = models.BooleanField(verbose_name=_("Is banned?"))
    can_edit_posts = models.BooleanField(verbose_name=_("Can edit posts?"))
    can_add_posts = models.BooleanField(verbose_name=_("Can add posts"))

    class Meta:
        app_label = "core"

    def __unicode__(self):
        return " - ".join([unicode(self.user), unicode(self.blog),
                           unicode(self.is_creator), unicode(self.is_admin), unicode(self.is_banned)])

