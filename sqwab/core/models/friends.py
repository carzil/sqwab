#(c) Andreev Alexander 2012 (aka Carzil)
from django.db import models
from sqwab.core.models.user import User

class Friendship(models.Model):
    user1 = models.ForeignKey(User, related_name="user1")
    user2 = models.ForeignKey(User, related_name="user2")
    is_accepted = models.BooleanField(default=False)

    class Meta:
        app_label = "core"

    def __unicode__(self):
        return unicode(self.user1) + " -> " + unicode(self.user2) + " (" + unicode(self.is_accepted) + ")"

