#(c) Andreev Alexander 2012 (aka Carzil)
import django.db.models as models
from sqwab.core.models.user import User
from sqwab.core.models.blog import Blog
from sqwab.core.views.utils import get_random_string
from django.utils.translation import ugettext as _

def get_image_name(instance, filename):
    salt = get_random_string(length=30)
    uname = instance.author.username
    fname = "_".join([salt, filename])
    return "uploads/%s/posts/%s" % (uname, fname)

POST_TYPES = (
    ("p", _("Post")),
    ("l", _("Link")),
    ("ll", _("Links")),
    ("i", _("Image")),
    ("v", _("Video")),
    ("n", _("Note")),
    ("pl", _("Poll"))
)

class Post(models.Model):
    type = models.CharField(max_length=2, choices=POST_TYPES)
    title = models.CharField(max_length=50)
    text = models.TextField()
    tags = models.TextField()
    author = models.ForeignKey(User)
    blog = models.ForeignKey(Blog, blank=False)
    publication_date = models.DateTimeField(auto_now_add=True)
    last_changed_date = models.DateTimeField(auto_now=True)
    link = models.URLField(blank=True)
    image = models.ImageField(upload_to=get_image_name, blank=True, null=True)
    is_hidden = models.BooleanField()
    is_posted = models.BooleanField()
    question = models.TextField(blank=True)

    class Meta:
        app_label = "core"

    def __unicode__(self):
        return "(" + " - ".join([unicode(self.type), unicode(self.title),
                              unicode(self.author), unicode(self.blog), unicode(self.publication_date)]) + ")"

    def get_absolute_url(self):
        return "/blog/post/%s" % (str(self.id),)
