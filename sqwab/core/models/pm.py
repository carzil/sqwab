#(c) Andreev Alexander 2012 (aka Carzil)
from django.db import models
from sqwab.core.models.user import User

class PM(models.Model):
    theme = models.CharField(max_length=500, blank=True)
    text = models.TextField()
    sending_date = models.DateField(auto_now_add=True)
    creator = models.ForeignKey(User)
    is_read = models.BooleanField()

    class Meta:
        app_label = "core"

    def __unicode__(self):
        return "(" + " - ".join([unicode(self.theme), unicode(self.text), unicode(self.sending_date), unicode(self.creator),
                          unicode(self.is_read)]) + ")"


class PMToUser(models.Model):
    dst_user = models.ForeignKey(User)
    pm = models.ForeignKey(PM)

    class Meta:
        app_label = "core"

    def __unicode__(self):
        return " - ".join([unicode(self.dst_user), unicode(self.pm)])