#(c) Andreev Alexander 2012 (aka Carzil)
from django.db import models
from sqwab.core.models.user import User
from sqwab.core.models.post import Post
from mptt.models import MPTTModel, TreeForeignKey

class Comment(MPTTModel):
    post = models.ForeignKey(Post)
    author = models.ForeignKey(User)
    text = models.TextField()
    submit_time = models.DateTimeField(auto_now_add=True)

    parent = TreeForeignKey("self", null=True, blank=True, related_name="children")


    class Meta:
        app_label = "core"

    def __unicode__(self):
        return " - ".join([
            unicode(self.author),
            unicode(self.post),
            unicode(self.submit_time)
        ])

