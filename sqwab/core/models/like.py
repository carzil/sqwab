#(c) Andreev Alexander 2012 (aka Carzil)
from django.db import models
from sqwab.core.models.user import User
from sqwab.core.models.post import Post

class PostLike(models.Model):
    user = models.ForeignKey(User)
    post = models.ForeignKey(Post)

    class Meta:
        app_label = "core"

    def __unicode__(self):
        return unicode(self.user) + " like " + unicode(self.post)

