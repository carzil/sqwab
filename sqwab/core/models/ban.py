#(c) Andreev Alexander 2012 (aka Carzil)
from django.db import models
from sqwab.core.models.user import User

class UserBan(models.Model):
    user = models.ForeignKey(User)
    reason = models.TextField()

    class Meta:
        app_label = "core"
