from sqwab.core.models.stats import UserBlogActivity

def add_uba(user, blog):
    uba, is_created = UserBlogActivity.objects.get_or_create(blog=blog, user=user)
    if not is_created:
        if uba.cnt:
            uba.cnt += 1
        else:
            uba.cnt = 1
    else:
        uba.cnt = 1
    uba.save()
