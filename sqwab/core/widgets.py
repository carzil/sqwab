#(c) Andreev Alexander 2012 (aka Carzil)
from django.forms import widgets
from django.forms.util import flatatt
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _

class CaptchaInput(widgets.Widget):
    def render(self, name, value, attrs=None):
        final_attrs = self.build_attrs(attrs, name=name)
        html = """<input type="text"%s/><br>
        <div class="captcha_container"><img src="%s" class="captcha"></div><a href="#" class="captcha_refresh">%s</a><br>
        """ \
            % (flatatt(final_attrs), "/captcha/", _("Show new code"))
        return mark_safe(html)