#(c) Andreev Alexander 2012 (aka Carzil)
from django.core.exceptions import ObjectDoesNotExist
from django.forms import ValidationError
from django.forms.fields import Field
from django.utils.translation import ugettext as _
from sqwab.core.models.captcha import Captcha
from hashlib import md5

class CaptchaField(Field):

    def clean(self, value):
        value = str(value).lower()
        super(CaptchaField, self).clean(value)
        m = md5()
        m.update(value)
        solution = m.hexdigest()
        try:
            captcha = Captcha.objects.get(solution=solution)
            captcha.delete()
        except ObjectDoesNotExist:
            raise ValidationError(_("You entered incorrect code, please enter again"))
        return value


