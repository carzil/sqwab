from django.test import TestCase, Client
from core.forms.post import PostAddForm
from sqwab.core.models.blog import Blog


def log_in(client):
    return client.post("/login/", {
        "username": "root",
        "password": "123"
    })



class LoginTest(TestCase):
    fixtures = ["initial.json"]

    def setUp(self):
        self.client = Client()

    def test_get_main_unauth(self):
        r = self.client.get("/")
        self.assertEquals(r.status_code, 200)

    def test_login_redirect(self):
        r = self.client.get("/blog/")
        self.assertRedirects(r, "/login/?next=/blog/")

    def test_login(self):
        r = log_in(self.client)
        self.assertRedirects(r, "/")

class BlogTest(TestCase):
    fixtures = ["initial.json"]

    def setUp(self):
        self.client = Client()
        log_in(self.client)

    def test_get_blog(self):
        r = self.client.get("/blog/")
        self.assertTemplateUsed(r, "blog/blog.html")

    def test_get_post(self):
        r = self.client.get("/blog/post/1/")
        self.assertTemplateUsed(r, "blog/post/post.html")

    def test_add_post(self):
        r = self.client.post("/blog/1/add/post/", {
            "title": "Hello, from Tests ;)",
            "text": "Hello!",
            "tags": "hello, test",
        })
        self.assertRedirects(r, "/blog/1/post/38") # WARNING!!!

    def test_add_link(self):
        r = self.client.post("/blog/1/add/link/", {
            "title": "Hello, from Tests ;)",
            "text": "Hello!",
            "tags": "hello, test",
            "link": "http://ya.ru"
            })
        self.assertRedirects(r, "/blog/1/post/39/") # WARNING!!!

    def test_add_post_fail(self):
        r = self.client.post("/blog/1/add/post/", {
            "title": "",
            "text": "",
            "tags": "",
            })
        self.assertEqual(r.status_code, 200)
