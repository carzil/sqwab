#(c) Andreev Alexander 2012 (aka Carzil)
from django import template
from django.core.exceptions import ObjectDoesNotExist
from sqwab.core.models.user import UserProfile

register = template.Library()

@register.filter(is_save=True)
def get_user_photo_link(user_id):
    try:
        uphoto = UserProfile.objects.get(user__id=int(user_id))
        return "/media/%s" % (uphoto.photo.name,)
    except (ObjectDoesNotExist, ValueError):
        return "/static/images/no_photo.png"
get_user_photo_link.is_save = True




