from django import template
from django.utils.safestring import mark_safe
from postmarkup import create
register = template.Library()

@register.filter(is_save=True)
def bbcodes_render(value):
    parser = create(render_unknown_tags=True)
    return mark_safe(parser(value))
