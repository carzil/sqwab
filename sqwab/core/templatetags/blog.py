#(c) Andreev Alexander 2012 (aka Carzil)
from django import template
from django.core.exceptions import ObjectDoesNotExist
from django.utils.safestring import mark_safe
from sqwab.core.models.like import PostLike
from sqwab.core.models.post import Post
from sqwab.core.models.blog import Blog, BlogMembership
from sqwab.core.models.user import User

register = template.Library()

@register.filter(is_save=True)
def get_number_of_memberships(bid):
    blog = Blog.objects.get(id=int(bid))
    number = blog.readers.count()
    return mark_safe(str(number))

@register.filter(is_save=True)
def get_number_of_posts(bid):
    blog = Blog.objects.get(id=int(bid))
    number = Post.objects.filter(blog=blog).count()
    return mark_safe(str(number))

@register.filter(is_save=True)
def can_edit_posts(bid, uid):
    user = User.objects.get(id=int(uid))
    blog = Blog.objects.get(id=int(bid))
    bmsh = BlogMembership.objects.get(user=user, blog=blog)
    return bmsh.can_edit_posts

@register.filter(is_save=True)
def like_post(uid, pid):
    post = Post.objects.get(id=int(pid))
    user = User.objects.get(id=int(uid))
    try:
        PostLike.objects.get(post=post, user=user)
    except ObjectDoesNotExist:
        return False
    return True

@register.filter(is_save=True)
def get_owner(blog):
    bmsh = BlogMembership.objects.get(blog=blog, is_creator=True)
    return bmsh.user
