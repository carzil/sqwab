# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
from django import template
from django.utils.http import urlquote_plus
from sqwab.core.views.utils import get_ending as ge
from sqwab.core.views.utils import format_likes as fl

register = template.Library()

@register.filter(is_save=True)
def cut(text, flen):
    text = unicode(text)
    if len(text) > (flen - 3):
        return "".join(text[:flen - 3], u"...")
    else:
        return text

@register.filter(is_save=True)
def split_tags(tags):
    return map(lambda x: unicode(x.strip()), tags.split(","))

@register.filter(is_save=True)
def urlquote(url):
    return unicode(urlquote_plus(url))

@register.filter(is_save=True)
def get_ending(n, e):
    e = e.split(",")
    return ge(n, e)

@register.filter(is_save=True)
def format_likes(n):
    return fl(n)

@register.filter(is_save=True)
def split(string, sep=" "):
    return unicode(string).split(sep)

