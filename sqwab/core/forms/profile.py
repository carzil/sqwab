# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
import django.forms as forms
from django.utils.translation import ugettext_lazy as _
from django.forms.extras.widgets import SelectDateWidget

BIRTH_YEAR_CHOICES = tuple(range(1900, 2013))

class ProfileEditForm(forms.Form):
    first_name = forms.CharField(label=_("First name"), max_length=500, required=False)
    last_name = forms.CharField(label=_("Last name"), max_length=500, required=False)
    birthday = forms.DateField(label=_("Your birthday"),
        widget=SelectDateWidget(years=BIRTH_YEAR_CHOICES),
        required=False
    )
    photo = forms.ImageField(label=_("Photo"), required=False)

