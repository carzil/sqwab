# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
import django.forms as forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

class SettingsForm(forms.Form):
    lang = forms.ChoiceField(label=_("Language"), choices=settings.LANGUAGES)
