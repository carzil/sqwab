# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
from django.contrib.auth import authenticate, login
from django.core.exceptions import ObjectDoesNotExist
import django.forms as forms
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _

class AuthenticationForm(forms.Form):
    username = forms.CharField(label=_("Login"), max_length=30)
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput)

    error_messages = {
        "invalid_login": _("Invalid login!"),
        "invalid_password": _("Invalid password!"),
        "inactive": _("This account is inactive!"),
    }

    fields = (password, username)

    def __init__(self, *args, **kwargs):
        self.user = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)

    def clean_password(self):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")
        auth = authenticate(username=username, password=password)
        if not auth:
            raise forms.ValidationError(self.error_messages["invalid_password"])
        else:
            self.user = auth
        return password

    def clean_username(self):
        username = self.cleaned_data.get("username")
        try:
            user = User.objects.get(username=username)
        except ObjectDoesNotExist:
            user = None
        if not user:
            raise forms.ValidationError(self.error_messages["invalid_login"])
        elif not user.is_active:
            raise forms.ValidationError(self.error_messages["inactive"])
        return username

    def login(self, request, next_url):
        login(request, self.user)
        next_url = next_url or "/"
        return redirect(next_url)

