# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
import django.forms as forms
from django.utils.translation import ugettext as _

class CommentForm(forms.Form):
    post = forms.IntegerField(widget=forms.HiddenInput)
    parent = forms.IntegerField(widget=forms.HiddenInput, required=False)
    comment = forms.CharField(label=_("Comment"), widget=forms.Textarea(attrs={"rows": 3, "cols": 40, "placeholder": _("Your comment here...")}))
