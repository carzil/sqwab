# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
from django.core.exceptions import ObjectDoesNotExist
import django.forms as forms
from django.contrib.auth import login, authenticate
from sqwab.core.models.blog import *
from django.utils.translation import ugettext_lazy as _
from sqwab.core.widgets import CaptchaInput
from sqwab.core.fields import CaptchaField

class RegistrationForm(forms.ModelForm):
    error_messages = {
        "duplicate_username": _("This nickname already exists!"),
        "duplicate_email": _("This email already exists!"),
        "password_mismatch": _("Passwords don't match!")
    }
    username = forms.RegexField(label=_("Login"), max_length=30,
        regex=r"[a-zA-Z0-9]{4,30}",
        error_messages={
            "invalid": _("Min four characters. Allowed only digits, latin letters and underline sign")
        },
        help_text=_("Required. 30 characters or fewer. Letters, digits and "
                    "@ . + - _ only.")

    )
    email = forms.EmailField(label=_("E-mail"))
    password1 = forms.CharField(label=_("Password"), widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirm"), widget=forms.PasswordInput)
    captcha = CaptchaField(label=_("Enter the following code"), widget=CaptchaInput)


    class Meta:
        model = User
        fields = ("username", "email")

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages["duplicate_username"])

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            User.objects.get(email=email)
        except ObjectDoesNotExist:
            return email
        raise forms.ValidationError(self.error_messages["duplicate_email"])


    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data["password2"]
        if password1 != password2:
            raise forms.ValidationError(self.error_messages["password_mismatch"])
        return password2

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.save()
        blog = Blog.objects.create(name=user.username.capitalize() + "'s blog", is_main=True)
        BlogMembership.objects.create(
            user=user,
            blog=blog,
            is_creator=True,
            is_admin=True,
            can_edit_posts=True,
            can_add_posts=True
        )
        return user

    def login_user(self, request):
        u = authenticate(username=self.cleaned_data["username"], password=self.cleaned_data["password1"])
        if u:
            login(request, u)


