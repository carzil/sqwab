# -*- coding=utf-8 -*-
#(c) Andreev Alexander 2012 (aka Carzil)
import django.forms as forms
from django.utils.translation import ugettext_lazy as _

class AddForm(forms.Form):
    title = forms.CharField(label=_("Title"), max_length=50)
    text = forms.CharField(label=_("Text"), widget=forms.Textarea)
    tags = forms.CharField(label=_("Tags"), max_length=500, required=False)


class PostAddForm(AddForm):
    pass

class ImageAddForm(AddForm):
    image = forms.ImageField(label=_("Image"), max_length=50)

class ImageEditForm(AddForm):
    image = forms.ImageField(label=_("Image"), max_length=50, required=False)

class LinkAddForm(forms.Form):
    title = forms.CharField(label=_("Title"), max_length=50)
    link = forms.URLField(label=_("Hyperlink"), max_length=500, required=False)
    text = forms.CharField(label=_("Text"), widget=forms.Textarea)
    tags = forms.CharField(label=_("Tags"), max_length=500, required=False)

