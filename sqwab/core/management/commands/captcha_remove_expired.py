#(c) Andreev Alexander 2012 (aka Carzil)
from datetime import datetime, timedelta
from django.core.management.base import NoArgsCommand
import sys
from sqwab.core.models.captcha import Captcha

class Command(NoArgsCommand):
    help = "Removes expired captcha"

    def handle_noargs(self, **options):
        qs = Captcha.objects.filter(generation_time__lt=datetime.now() - timedelta(minutes=10))
        cnt = qs.count()
        qs.delete()
        sys.stdout.write("Successfully deleted %d expired captcha\n" % cnt)

