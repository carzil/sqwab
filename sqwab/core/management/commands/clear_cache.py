#(c) Andreev Alexander 2012 (aka Carzil)
from django.core.management.base import NoArgsCommand
from django.core.cache import cache
from sys import stdout

class Command(NoArgsCommand):
    help = "Clear a cache"

    def handle_noargs(self, **options):
        stdout.write("Clearing cache... ")
        cache.clear()
        stdout.write("done\n")

