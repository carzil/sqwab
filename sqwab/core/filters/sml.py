#(c) Andreev Alexander (aka Carzil) 2011
from lxml import etree
from StringIO import StringIO

#--------Utils---------------

def escape(string):
    if not isinstance(string, str):
        string = str(string)
    return string.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;').replace('"', '&quot;')

#--------Exceptions----------

class SMLException(Exception):
    pass

#--------Nodes---------------

class SMLNode(object):
    def __init__(self, elem, tags):
        self.element = elem
        self.tags = tags

    def to_html(self):
        raise NotImplementedError

    def parse(self):
        raise NotImplementedError

    def get_node(self, element):
        return self.tags[element.tag](element)


class SMLRootNode(SMLNode):
    def parse(self):
        self.children = []
        self.get_node(self.element)

    def to_html(self):
        output = ""
        for i in self.children:
            output = "".join([output, i.to_html()])
        return output


class SMLCodeNode(SMLNode):
    def parse(self):
        self.text = escape(self.element.text)
        try:
            self.lang = element.attrib["lang"]
        except KeyError:
            self.lang = "no-highlight"

    def to_html(self):
        return """<pre><code class="language-%s">%s</code></pre>""" % (self.lang, self.text)

class SMLBoldNode(SMLNode):
    def __init__(self, element):
        self.element = element
        self.text = escape(self.element.text)

    def to_html(self):
        return """<b>%s</b>""" % self.text



#--------Parser----------

class SMLParser(object):
    def __init__(self):
        self.tags = {}

    def register_tag(self, tag, callback):
        self.tags[tag] = callback

    def __call__(self, text, *args, **kwargs):
        self.text = StringIO(text)
        parsed = etree.parse(self.text)
        root = SMLRootNode(parsed.getroot(), self.tags)
        return root.parse()

def register_default_tags(parser):
    parser.register_tag("code", SMLCodeNode)
    parser.register_tag("b", SMLBoldNode)

def parse_sml(text):
    parser = SMLParser()
    register_default_tags(parser)
    return parser(text)

if __name__ == "__main__":
    string = "<code lang='python'><b>def</b> hello(): print 'Hello!'</code>"
#    p = etree.parse(StringIO(string))
#    for i in p.iter():
#        print i.tag
    p = parse_sml("<code lang='python'><b>def</b> hello(): print 'Hello!'</code>")
    print p.to_html()


